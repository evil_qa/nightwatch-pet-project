module.exports = {

    '@tags': ['google', 'example'],
//    '@disabled': true,

    'Google advanced search' : function (browser) {
        const mainQuery = 'Programming languages';
        const mainQueryInputSelector = 'input[name="as_q"]';
        const languageDropdownOpenSelector = '#lr_button';  // --> # means by id
        const languageDropdownValueSelector = '.goog-menuitem[value="lang_ko"]';
        const lastUpdateDropdownOpenSelector = '#as_qdr_button';
        const lastUpdateDropdownValueSelector = '.goog-menuitem[value="y"]';
        const submitButtonSelector = '.jfk-button[type="submit"]'
        const resultsPageQuerySelector = `#searchform input[name="q"][value="${mainQuery}"]`;

        browser
            .url('https://www.google.com/advanced_search')
            .setValue(mainQueryInputSelector, mainQuery)
            .click(languageDropdownOpenSelector)
            .click(languageDropdownValueSelector)
            .click(lastUpdateDropdownOpenSelector)
            .click(lastUpdateDropdownValueSelector)
            .click(submitButtonSelector)
            .assert.urlContains('as_q=Programming+languages', 'Params: search query is correct')
            .assert.urlContains('lr=lang_ko', 'Params: language is - korean')
            .assert.urlContains('as_qdr=y', 'Params: time period is - last year')
            .assert.visible(resultsPageQuerySelector, 'UI: search request - Programming languages');

        browser.saveScreenshot('tests_output/google.png')
    }
}

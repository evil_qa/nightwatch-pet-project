# Nightwatch.js pet project

Project template and simple test examples

## Installation

Install required dependencies
including Selenium Server and `chromedriver`:

```
npm install
```

## Run tests

Execute tests:

```
npm test
```